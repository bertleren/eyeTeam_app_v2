import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

/*
  Generated class for the Device page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-device',
  templateUrl: 'device.html'
})
export class DevicePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }

  ionViewDidLoad() {

    document.getElementById('titleCard').innerHTML = '<div class="toolbar-title toolbar-title-ios" ng-reflect-klass="toolbar-title" ng-reflect-ng-class="toolbar-title-ios">'+this.navParams.get('device')+'</div>';
    console.log('ionViewDidLoad DevicePage');
    setTimeout(function(){
      var el = document.getElementById('fadeArrow');
      if(el) {
        el.className += el.className ? ' animated fadeIn' : 'animated fadeIn';
      }
    }, 200);
    setTimeout(function(){
      var el = document.getElementById('activeToggle');
      if(el) {
        el.className += el.className ? ' animated fadeIn' : 'animated fadeIn';
      }
    }, 200);
  }

  back(){
    document.getElementById("fadeArrow").style.display = "none";
    document.getElementById("activeToggle").style.display = "none";
    this.navCtrl.pop();
  }

  getLocation(){
    console.log("location");
  }


}
