import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { DevicePage } from '../device/device';
import { SettingsPage } from '../settings/settings';
import { ActionSheetController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { ServiceReader } from '../../providers/Service-reader';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';

/*
  Generated class for the Main page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-main',
  templateUrl: 'main.html',
  providers: [ServiceReader]
})

export class MainPage {
  storage: Storage;
  http: Http;

  constructor(public navCtrl: NavController, public navParams: NavParams, public actionSheetCtrl: ActionSheetController, private toastCtrl: ToastController, private ServiceReader: ServiceReader, storage: Storage, http: Http) {
    this.storage = storage;
    this.http = http;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MainPage');
  }

  gotoDevice(data){

    this.navCtrl.push(DevicePage, {
        device: data
    });

  }



  pressEvent(data){
    console.log("pressed");
    let actionSheet = this.actionSheetCtrl.create({
     title: 'Quick settings',
     buttons: [
       {
         text: 'Deactivate alarm',
         role: 'destructive',
         handler: () => {
           console.log('Alarm deactivated');
           let toast = this.toastCtrl.create({
            message: 'Alarm was deactivated',
            duration: 10000,
            position: 'bottom',
            showCloseButton: true,
            closeButtonText: 'OK'
          });

          toast.onDidDismiss(() => {
            console.log('Dismissed toast');
          });

          toast.present();
         }
       },
       {
         text: 'Get location',
         handler: () => {
           console.log('Alarm location');
         }
       },
       {
         text: 'More settings',
         handler: () => {
           this.navCtrl.push(DevicePage, {
              device: data
          });
         }
       },
       {
         text: 'Cancel',
         role: 'cancel',
         handler: () => {
           console.log('Cancel clicked');
         }
       }
     ]
   });

   actionSheet.present();
  }

  gotoSettintgs(){
    this.navCtrl.push(SettingsPage);
  }

}
