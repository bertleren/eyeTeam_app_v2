import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MainPage } from '../main/main';
import { ServiceReader } from '../../providers/Service-reader';
import { ToastController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  providers: [ServiceReader]
})
export class HomePage {
  public rootPage;
  public isEnabled:boolean = true;
  key: string;
  storage: Storage;
  auth;
  phoneId;
  

  constructor(public navCtrl: NavController, private ServiceReader: ServiceReader, public toastCtrl: ToastController,public http: Http, storage: Storage) {
    this.storage = storage;
  }

  ionViewDidLoad() {
    this.storage.ready().then(()=>{

      this.http.get("http://holydb.com/eyeteam/appVer.php").subscribe(data => {
        this.storage.set("appVer", data.json().appVer);
      });

      
      this.storage.get('authenticator').then((val) => {
        this.auth = val;
        this.storage.get('phoneId').then((val) => {
          this.phoneId = val;

          // do check for login
          if(this.auth == null || this.phoneId == null){
            //do nothing
            document.getElementById("overlay").style.display = "none";
          } else {
            //webservice check authentication for device

            this.http.get("http://holydb.com/eyeteam/loginDevice.php?unlock="+this.phoneId).subscribe(data => {
              if(data.json().error == "none"){

                if(data.json().authenticator == this.auth){
                  var nav = this.navCtrl
                  nav.setRoot(MainPage);
                } else {
                  document.getElementById("overlay").style.display = "none";
                }

              } else {
                document.getElementById("overlay").style.display = "none";
              }
            }, error => {
                console.log(JSON.stringify(error.json()));
            });
          }
        });
      });

    });
  }

  displayPop(){
    let toast = this.toastCtrl.create({
      message: 'Du kan finde din aktiveringskode på din bruger, på eyeTeam\'s hjemmeside. Når du opretter en ny enhed vil du kunne se en aktiveringskode!',//change description
      showCloseButton: true,
      dismissOnPageChange: true,
      closeButtonText: "Ok"
    });
    toast.present();
 }

  content = "Login";
  generatedId = "";
  login() {
    if(this.isEnabled){
      this.isEnabled = false;
      document.getElementById("loginText").style.display = "none";
      document.getElementById("loginSpinner").style.display = "block";

      this.generatedId = this.makeid();
      this.http.get("http://holydb.com/eyeteam/unlockDevice.php?id="+this.generatedId+"&unlock="+this.key).subscribe(data => {
          if(data.json().error == "none"){

            var authKey = data.json().authenticator;

            this.storage.ready().then(()=>{

              this.storage.set("phoneId", this.generatedId).then(()=>{

                this.storage.set("authenticator", authKey).then(()=>{

                  var nav = this.navCtrl
                  nav.setRoot(MainPage);
                  
                });

              });

            });

          } else {
              let toast = this.toastCtrl.create({
                message: data.json().error,
                duration: 3000,
                position: 'bottom'
              });
            
              document.getElementById("loginText").style.display = "block";
              document.getElementById("loginSpinner").style.display = "none";
              this.isEnabled = true;

              toast.present();
          }
        }, error => {
            console.log(JSON.stringify(error.json()));
        });
    }
    
  }

  makeid()
  {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

      for( var i=0; i < 5; i++ )
          text += possible.charAt(Math.floor(Math.random() * possible.length));

      return text;
  }

}
