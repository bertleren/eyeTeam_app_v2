import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Http } from '@angular/http';

/*
  Generated class for the Settings page.

  See http://ionicframework.com/docs/v2/components/#navigation for more info on
  Ionic pages and navigation.
*/
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html'
})
export class SettingsPage {
  storage: Storage;
  http: Http;
  vacMode: any;
  lang: string;
  da: boolean = false;
  en: boolean = false;
  de: boolean = false;
  temp: boolean;
  tempUnit: string;
  appVersion: string;
  curAppVersion: string = "1.0";//Set before new version release

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController, storage: Storage, http: Http) {
    this.storage = storage;
    this.http = http;
  }

  ionViewDidLoad() {
    setTimeout(function(){
      var el = document.getElementById('fadeArrow');
      if(el)
        el.className += el.className ? ' animated fadeIn' : 'animated fadeIn';
    }, 200);

    //Get all storage settings
    this.storage.ready().then(()=>{

      this.storage.get('vacMode').then((val) => {
        this.vacMode = val;
      });

      this.storage.get('phoneId').then((val) => {
        document.getElementById("phoneId").innerHTML=val;
      });

      this.storage.get('lang').then((val) => {
        if(val == "de"){
          this.lang = "Deutsch";
          this.de = true;
        } else if(val == "da"){
          this.lang = "Dansk";
          this.da = true;
        } else {
          this.lang = "English";
          this.en = true;
        }
      });

      this.storage.get('temp').then((val) => {
        this.temp = val;
        if(val) this.tempUnit = "F";
        else this.tempUnit = "C";
      });

      this.storage.get('appVer').then((val) => {
        this.appVersion = val;
      });

    });

  }

  tempChange(){
    this.storage.ready().then(()=>{
      if(this.temp){
          this.temp = false;
          this.tempUnit = "C";
          this.storage.set("temp", false);
      } else {
          this.temp = true;
          this.tempUnit = "F";
          this.storage.set("temp", true);
      }
    });
  }

  back(){
    document.getElementById("fadeArrow").style.display = "none";
    this.navCtrl.pop();
  }

  toggleVac(){
    this.storage.ready().then(()=>{
      if(this.vacMode){
        this.storage.set("vacMode", false);
        this.vacMode = false;
      } else {
        this.storage.set("vacMode", true);
        this.vacMode = true;
      }
    });
  }

  langSelect() {
    let alert = this.alertCtrl.create();
    alert.setTitle('Languege selection');

    alert.addInput({
      type: 'radio',
      label: 'English',
      value: 'en',
      checked: this.en
    });

    alert.addInput({
      type: 'radio',
      label: 'Dansk',
      value: 'da',
      checked: this.da
    });

    alert.addInput({
      type: 'radio',
      label: 'Deutsch',
      value: 'de',
      checked: this.de
    });

    alert.addButton('Cancel');
    alert.addButton({
      text: 'OK',
      handler: data => {
        this.storage.ready().then(()=>{
          this.storage.set("lang", data);
          if(data == "de") this.lang = "Deutsch";
          else if(data == "da") this.lang = "Dansk";
          else this.lang = "English";
        });
      }
    });
    alert.present();
  }

}
